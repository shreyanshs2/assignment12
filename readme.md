# Screenshots of outputs

## Output1 - form page

![Output1](./resources/screenshots/output1.jpg?raw=true "Form page")

## Output2 - wrong data input

![Output2](./resources/screenshots/output2.jpg?raw=true "Wrong Input")

## Output3 - Error Message for Wrong Input

![Output3](./resources/screenshots/output3.jpg?raw=true "Error Message")

## Output4 - Correct Input in form

![Output4](./resources/screenshots/output5.jpg?raw=true "Correct Input")

## Output5 - Correct Input Takes to Info Page

![Output5](./resources/screenshots/output4.jpg?raw=true "User Info Page")